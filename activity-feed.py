from bs4 import BeautifulSoup
import urllib2
import re


activitySoup = BeautifulSoup(urllib2.urlopen('https://bitbucket.org/Tridude/profile/activity').read())

activityRoot = activitySoup.find('div', {'class':'newsfeed'})

for activity in activityRoot.find_all('article'):
   #print activity.p.a.string
   print re.sub(r'\s+', r' ', activity.p.text)[1:]
   print activity.time.string